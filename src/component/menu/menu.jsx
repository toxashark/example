import React from 'react';
import './style.css'


const Menu = (props) => {
    return (
        <div className={props.active ? 'menu active' : 'menu'} onClick={()=>props.setactive(false)}>
            <div className='blur'>
                <div className="menu__content" onClick={e=>e.stopPropagation()}>
                    <div className='menu__header'>
                        {props.header}
                    </div>
                    <ul>
                        {props.items.map(item =>
                            <li>
                                <a href={item.href}>{item.value}</a>
                                <span className="material-icons">{item.icon}</span>
                            </li>)}
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default Menu;