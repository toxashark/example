import React from "react";
import './styleModal.css'




const Modal = ({active,setActive,children}) => {
    return (
        <div className={active ? "modal active" : 'modal'} onClick={()=>setActive(false)} >
            <div className={active ? "content-modal active" : 'content-modal'} onClick={e=>e.stopPropagation()}>
                {children}
            </div>
        </div>
    )
}


export default Modal
