import React,{useState,useEffect} from 'react';
import * as axios from 'axios'
import './style.css'



function Test(props) {
        const people = [{name:'anton',id:'1'},{name:'Dima',id:'2'},{name:'Sasha',id:'2'},]
        // const [contries,setContries] = useState([])
        // const getContries = () => {
        //     debugger;
        //     axios.get(`https://restcountries.eu/rest/v2/all`).then((response)=>{
        //         setContries(response.data)
        //     })
        // }
        // useEffect(()=>{
        //     getContries()
        // },[])
        const [state,setState] = useState('')
        const filterMasive = people.filter(ludi=>{
            return ludi.name.toLowerCase().includes(state.toLowerCase())
        })
            //toLowerCase переводит в нижний реестр буквы
            // includes Сравнивает значение с масивом есть ли оно там
        const [openList,setOpenList] = useState(true)

        const InputClick = () =>{
                setOpenList(true)
        }

        const itemsput = (event) =>{
                setState(event.target.textContent)
                setOpenList(!openList)
        }

        return (
            <div>
                {state}
                <input onClick={InputClick} onChange={(event)=>setState(event.target.value)}
                       value={state} type="text"/>
                <ul className="autoComplete">
                        {state && openList ? filterMasive.map(post=><li onClick={itemsput} className="complete_content">{post.name}</li>) : null}
                </ul>
                <div>
                    {filterMasive.map(post=><div>
                        {post.name}
                    </div>)}
                </div>
            </div>
        );
}

export default Test;
